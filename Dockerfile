FROM registry.gitlab.com/fdroid/ci-images:server-latest
MAINTAINER team@f-droid.org

run apt-get update && apt-get install -y --no-install-recommends \
		python3-pip \
		python3-venv \
		pyflakes \
		pep8 \
        && apt-get install -y --no-install-recommends -t jessie-backports \
		git \
		pylint3 \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

COPY test /
