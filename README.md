# F-Droid Repomaker CI image

These are _Docker_ images used in F-Droid's for Continuous Integration Tests for the [Repomaker](https://gitlab.com/fdroid/repomaker).  They are built via _gitlab-ci_ using _Docker_.

The completed images are then posted here:
https://gitlab.com/fdroid/ci-images-repomaker/container_registry
